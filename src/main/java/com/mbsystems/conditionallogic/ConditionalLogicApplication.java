package com.mbsystems.conditionallogic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConditionalLogicApplication {

    public static void main( String[] args ) {
        SpringApplication.run( ConditionalLogicApplication.class, args );
    }

}
