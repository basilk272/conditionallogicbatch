package com.mbsystems.conditionallogic.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBatchProcessing
public class ConditionalLogicBatchConfig {

    private final JobBuilderFactory jobBuilderFactory;

    private final StepBuilderFactory stepBuilderFactory;

    @Autowired
    public ConditionalLogicBatchConfig( JobBuilderFactory jobBuilderFactory, StepBuilderFactory stepBuilderFactory ) {
        this.jobBuilderFactory = jobBuilderFactory;
        this.stepBuilderFactory = stepBuilderFactory;
    }

    @Bean
    public Tasklet passTasklet() {
        return ( ( stepContribution, chunkContext ) -> {
            //return RepeatStatus.FINISHED;
            // bmk try this out when we get back an exception
            throw new RuntimeException( "This a failure" );
        });
    }

    @Bean
    public Tasklet successTasklet() {
        return ( ( stepContribution, chunkContext ) -> {
            System.out.println( "Success!" );
            return RepeatStatus.FINISHED;
        });
    }

    @Bean
    public Tasklet failTasklet() {
        return ( ( stepContribution, chunkContext ) -> {
            System.out.println( "Failure!" );
            System.out.println( "Basil you failed MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM");
            return RepeatStatus.FINISHED;
        });
    }

    @Bean
    public Step firstStep() {
        return this.stepBuilderFactory.get( "firstStep" )
                        .tasklet( passTasklet() )
                        .build();
    }

    @Bean
    public Step successStep() {
        return this.stepBuilderFactory.get( "successStep" )
                        .tasklet( successTasklet())
                        .build();
    }

    @Bean
    public Step failureStep() {
        return this.stepBuilderFactory.get( "failureStep" )
                        .tasklet( failTasklet() )
                        .build();
    }
    @Bean
    public Job job() {
        return this.jobBuilderFactory.get( "ConditionalLogicBatch" )
                        .start( firstStep() )
                        .on( "FAILED" ).to( failureStep() )
                        .from( firstStep() ).on( "*" ).to( successStep() )
                        .end()
                        .build();
    }
}
